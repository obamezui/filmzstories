<?php 
namespace Filmzstories\FilmzBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 *Category
 * 
 *@ORM\Entity
 *@ORM\Table(name="category")
 */
class Category{
/**
 *@ORM\Id
 *@ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
protected $id;
/**
 *@ORM\Column(type="string",length=50)
 *
 *@Assert\NotBlank()
 */
protected $title;

/**
 *@ORM\OneToMany(targetEntity="Movie",mappedBy="category")
 */
private $movies;

 public function __construct()
    {
        $this->movies = new ArrayCollection();
    }

    /**
     * toString
     * @return string
     */
    public function __toString() 
    {
        return $this->title;
    }

     /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add movie
     *
     * @param \Filmzstories\FilmzBundle\Entity\Movie $movie
     *
     * @return Category
     */
    public function addMovie(\Filmzstories\FilmzBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \Filmzstories\FilmzBundle\Entity\Movie $movie
     */
    public function removeMovie(\Filmzstories\FilmzBundle\Entity\Movie $movie)
    {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}
