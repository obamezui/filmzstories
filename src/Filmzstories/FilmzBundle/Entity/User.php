<?php 
namespace Filmzstories\FilmzBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * User
 * 
 *@ORM\Entity
 *@ORM\Table(name="User")
 */
class User implements UserInterface{
/**
 *@ORM\Column(type="guid")
 *@ORM\Id
 * @ORM\GeneratedValue(strategy="UUID")
 */
protected $id;
/**
 *@ORM\Column(type="string",length=50)
 *
 *@Assert\NotBlank()
 */
protected $username;
/**
 *@ORM\Column(type="string",length=50)
 *
 *@Assert\NotBlank()
 */
protected $firstname;
/**
 *@ORM\Column(type="string",length=36)
 *
 *@Assert\NotBlank()
 */
protected $login;
/**
 *@ORM\Column(type="string",length=64)
 *
 *@Assert\NotBlank()
 */
protected $password;


    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

     /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

     /**
     * Set firstname
     *
     * @param string $ursername
     *
     * @return User
     */
    public function seUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param \password $password
     *
     * @return User
     */
    public function setPassword( $password)
    {
        $this->password = password_hash($password,PASSWORD_BCRYPT);

        return $this;
    }

    /**
     * Get password
     *
     * @return \password
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt(){
        return null;
    }

    public function getRoles(){
        return array('ROLE_USER');
    }

    public function eraseCredentials(){

    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
}
