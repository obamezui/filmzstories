<?php

namespace Filmzstories\FilmzBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Movie
 *
 * @ORM\Table(name="movie")
 * @ORM\Entity
 */
class Movie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;

     /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="release_year", type="string", length=50)
     */
    private $release_year;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="description", type="string", length=400)
     */
    private $description;



      /**
     * @var string $image
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @Assert\File(maxSize = "2048k", mimeTypesMessage = "Entrer une image valide")
     */
    private $picture;

     /**
     * @ORM\ManyToOne(targetEntity="Category")
     *@ORM\JoinColumn(name="category_id",referencedColumnName="id")
     */
    private $category;

    /**
    *@ORM\OneToMany(targetEntity="Comment",mappedBy="movie")
    */
    private $comments;


    /**
     * @ORM\ManyToMany(targetEntity="Actor", inversedBy="movie")
     * @ORM\JoinTable(name="actor_movie")
     */
    private $actors;

       public function __construct()
        {
            //$this->comments = new ArrayCollection();
            //$this->actors = new ArrayCollection();
        }

      public function __toString() 
    {
        return $this->title;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set releaseYear
     *
     * @param string $releaseYear
     *
     * @return Movie
     */
    public function setReleaseYear($releaseYear)
    {
        $this->release_year = $releaseYear;

        return $this;
    }

    /**
     * Get releaseYear
     *
     * @return string
     */
    public function getReleaseYear()
    {
        return $this->release_year;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Movie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category
     *
     * @param \Filmzstories\FilmzBundle\Entity\Category $category
     *
     * @return Movie
     */
    public function setCategory(\Filmzstories\FilmzBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Filmzstories\FilmzBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add comment
     *
     * @param \Filmzstories\FilmzBundle\Entity\Comment $comment
     *
     * @return Movie
     */
    public function addComment(\Filmzstories\FilmzBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \Filmzstories\FilmzBundle\Entity\Comment $comment
     */
    public function removeComment(\Filmzstories\FilmzBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add actor
     *
     * @param \Filmzstories\FilmzBundle\Entity\Actor $actor
     *
     * @return Movie
     */
    public function addActor(\Filmzstories\FilmzBundle\Entity\Actor $actor)
    {
        $this->actors[] = $actor;

        return $this;
    }

    /**
     * Remove actor
     *
     * @param \Filmzstories\FilmzBundle\Entity\Actor $actor
     */
    public function removeActor(\Filmzstories\FilmzBundle\Entity\Actor $actor)
    {
        $this->actors->removeElement($actor);
    }

    /**
     * Get actors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActors()
    {
        return $this->actors;
    }

     /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
 
    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set picture
     *
     * @param string $image
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }
 
    /**
     * Get picture
     *image
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }



}
