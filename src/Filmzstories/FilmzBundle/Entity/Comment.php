<?php 
namespace Filmzstories\FilmzBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Comment
 * 
 *@ORM\Entity
 *@ORM\Table(name="comment")
 */
class Comment{
/**
 *@ORM\Id
 *@ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
protected $id;
/**
 *@ORM\Column(type="string",length=200)
 *
 *@Assert\NotBlank()
 */
protected $content;

/**
 * @ORM\ManyToOne(targetEntity="Movie")
 * @ORM\JoinColumn(name="movie_id",referencedColumnName="id")
 */
private $movie;




    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set movie
     *
     * @param \Filmzstories\FilmzBundle\Entity\Movie $movie
     *
     * @return Comment
     */
    public function setMovie(\Filmzstories\FilmzBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \Filmzstories\FilmzBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }
    
}
