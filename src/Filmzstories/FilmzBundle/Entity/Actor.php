<?php

namespace Filmzstories\FilmzBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Actor
 *
 * @ORM\Table(name="actor")
 * @ORM\Entity
 */
class Actor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    private $first_name;

     /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="last_name", type="string", length=50)
     */
    private $last_name;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="description", type="string", length=400)
     */
    private $description;

     /**
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="actors")
     */
    private $movies;

    public function __construct() {
        $this->movies = new ArrayCollection();
    }
    public function __toString() 
    {
        return $this->first_name;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Actor
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Actor
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Actor
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add movie
     *
     * @param \Filmzstories\FilmzBundle\Entity\Movie $movie
     *
     * @return Actor
     */
    public function addMovie(\Filmzstories\FilmzBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \Filmzstories\FilmzBundle\Entity\Movie $movie
     */
    public function removeMovie(\Filmzstories\FilmzBundle\Entity\Movie $movie)
    {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}
