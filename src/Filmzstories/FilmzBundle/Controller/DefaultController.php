<?php

namespace Filmzstories\FilmzBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FilmzstoriesFilmzBundle:Default:index.html.twig');
    }
}
