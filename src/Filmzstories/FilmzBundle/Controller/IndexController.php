<?php

namespace Filmzstories\FilmzBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Filmzstories\FilmzBundle\Entity\User;
use Filmzstories\FilmzBundle\Form\UserType;
use Filmzstories\FilmzBundle\Entity\Movie;
use Filmzstories\FilmzBundle\Form\MovieType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class IndexController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('FilmzstoriesFilmzBundle:Movie')->findAll();
        return $this->render('FilmzstoriesFilmzBundle:Index:index.html.twig',array('movies' => $movies,));
    }

    public function inscriptionAction()
    {
    	$user = new User();
    	$form = $this->createForm(UserType::class,$user,array(
    		'action'=>$this->generateUrl('index_adduser'),
    		'method'=>'POST'

    		));
    	$form->add('submit',SubmitType::class,array('label'=>'valider'));
        return $this->render('FilmzstoriesFilmzBundle:Index:inscription.html.twig',array('form'=>$form->createView()));
    }

     public function showAction(Movie $movie)
    {
       

        return $this->render('FilmzstoriesFilmzBundle:Index:show.html.twig', array(
            'movie' => $movie
        ));
    }

    public function adduserAction()
    {
    	/*$user = new User();
    	$form = new createForm(new UserType(),$user,array(
    		'action'=>$this->generateUrl('index_adduser'),
    		'method'=>'POST'

    		));
    	$form->add('submit','submit',array('label'=>'valider'));
        return $this->render('FilmzstoriesFilmzBundle:Index:inscription.html.twig',array('form'=>$form->createView()));*/
    }
}
