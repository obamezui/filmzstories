<?php

namespace Filmzstories\FilmzBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Filmzstories\FilmzBundle\Entity\Movie;
use Filmzstories\FilmzBundle\Form\MovieType;

/**
 * Movie controller.
 *
 */
class MovieController extends Controller
{
    /**
     * Lists all Movie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('FilmzstoriesFilmzBundle:Movie')->findAll();

        return $this->render('movie/index.html.twig', array(
            'movies' => $movies,
        ));
    }

    /**
     * Creates a new Movie entity.
     *
     */
    public function newAction(Request $request)
    {
        $movie = new Movie();
        $form = $this->createForm('Filmzstories\FilmzBundle\Form\MovieType', $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $movie->getPicture();
            $em = $this->getDoctrine()->getManager();
            $picName = $movie->getPicture()->getClientOriginalName();
            $movie->setImage($picName);
            $em->persist($movie);
            $em->flush();
            // Move the file to the directory where brochures are stored
            $imageDir = $this->container->getParameter('kernel.root_dir').'/../web/images/movies';
            $image->move($imageDir, $picName);
            return $this->redirectToRoute('cockpit_movie_show', array('id' => $movie->getId()));
        }

        return $this->render('movie/new.html.twig', array(
            'movie' => $movie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Movie entity.
     *
     */
    public function showAction(Movie $movie)
    {
        $deleteForm = $this->createDeleteForm($movie);

        return $this->render('movie/show.html.twig', array(
            'movie' => $movie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Movie entity.
     *
     */
    public function editAction(Request $request, Movie $movie)
    {
        $deleteForm = $this->createDeleteForm($movie);
        $editForm = $this->createForm('Filmzstories\FilmzBundle\Form\MovieTypeEdit', $movie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $image = $movie->getPicture();
            
            if($image!= null){
                $imageDir =$this->container->getParameter('kernel.root_dir').'/../web/images/movies/';
                unlink($imageDir.$movie->getImage());
                $em = $this->getDoctrine()->getManager();
                $picName = $movie->getPicture()->getClientOriginalName();
                $movie->setImage($picName);
                $em->persist($movie);
                $em->flush();

                  // Move the file to the directory where brochures are stored
                $imageDir = $this->container->getParameter('kernel.root_dir').'/../web/images/movies';
                $image->move($imageDir, $picName);  

            }
            else{

                $em = $this->getDoctrine()->getManager();
                $em->persist($movie);
                $em->flush();
            }


            return $this->redirectToRoute('cockpit_movie_edit', array('id' => $movie->getId()));
        }

        return $this->render('movie/edit.html.twig', array(
            'movie' => $movie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Movie entity.
     *
     */
    public function deleteAction(Request $request, Movie $movie)
    {
        $form = $this->createDeleteForm($movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageDir =$this->container->getParameter('kernel.root_dir').'/../web/images/movies/';
            unlink($imageDir.$movie->getImage());
            $em = $this->getDoctrine()->getManager();
            $em->remove($movie);
            $em->flush();
        }

        return $this->redirectToRoute('cockpit_movie_index');
    }

    /**
     * Creates a form to delete a Movie entity.
     *
     * @param Movie $movie The Movie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Movie $movie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cockpit_movie_delete', array('id' => $movie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
