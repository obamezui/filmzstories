<?php

namespace Filmzstories\FilmzBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Filmzstories\FilmzBundle\Entity\Movie;
use Filmzstories\FilmzBundle\Form\MovieType;

/**
 * Movie controller.
 *
 */
class CockpitController extends Controller
{
    /**
     * Lists all Movie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('FilmzstoriesFilmzBundle:Movie')->findAll();

        return $this->render('movie/index.html.twig', array(
            'movies' => $movies,
        ));
    }
}
